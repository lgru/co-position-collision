TeamC
Contents
[hide]

    1 Perspective
        1.1 Process Analysis
            1.1.1 Before or During
        1.2 What does it all mean
        1.3 What do we do about it now

Perspective

We will look at process and action.

To begin, we look at Lissitzky because the process for movable type is interesting for its foreign-ness to our experience of practice.

This is our first sample:
Lissitzky 05.jpg
Process Analysis

Our first thought is the multi-lingual content. Many languages but only one style of quotation marks: German „primary“ quotation marks (which in Russian are the secondary quotes). However, the 'baseline' of some quotes are irregular. On the German it is the left „ and in French it is the right “. In Cyrillic (and the second French) the baselines stabilize. We consider whether this is a conscious choice or if he ran out of full quote glyphs and was forced to use two single quote glyphs. If it were digital, we would know that it was on purpose.

Movable type (likely) involves considerable forethought. We look at the most stylized section, in the top-left, and notice the variability within the grid. The name of a magazine is set in three languages, with the Cyrillic version occupying the most visual space in an ascending ladder configuration.

This tension of multiple languages continues throughout. On the one hand, the German description of the left-hand appears first in the lower portion of the layout, then French, then Russian.
Before or During

Quotation marks.png

Gutters.png

We wonder how it went during this time of movable type to make a magazine. What was, if it existed, the division of labor between designer and typesetter? How much of this design was envisioned before type was being laid into its frame? Did Lissitzky lay it out with his own hand, or did he provide a typesetter with a sketch of the expected output? Would this mirror somehow the web designer who hands an AI or PSD file to a front-end programmer?

It doesn't seem to be likely that the layout was performed in a Monotype-style system, but that is perhaps a prejudice we bring, thinking that these early industrial machines could not perform the complexity of the top-left grid.

From Pierre H: It is actually likely that the column texts were composed in Mono/Linotype style, while the bold 'object' in the columns and the complex layout in the top-left.

We can look at the DTP revolution as significantly altering the relationship of the designer to canvas. As an example, Aldus Manutius had a number of assistants but was in charge of the final design. But without the assistance he would have produced fewer books. A DTP designer has some significant support 'ahead of time' in the form of software developers, but can now control (in possibly obsessive) detail.

Even with this new relationship of designer to 'canvas', its not to be said that the designer operates outside of external technical support (the entire software is what enables the relative freedom, for instance; education and inspiration from other designers is another example).

The width of the gutters does not seem to vary, though the width of the rules between the gutters does. This is a paper-based constraint, because changing gutter width would impact text column width or else push the text from the page.
What does it all mean

Constrained by the page

Now the layout can be ever-expanding (random snapshots). Based on the presentation, the layout can/will be different. In that way what is viewed is not the "actual" layout but then snapshots of potential variations of a layout (or is it no longer singular, and becomes layouts?)

We should respect the power of the canvas to flatten the distance between designer and design. Even if it is a PDF meant to become a book, any page of that book displays what was designed into the PDF through the canvas. Intermediate steps can still affect the output (printer ability/quality, color standards (or lack of), etc), however these would not define the layout in the same way as the construction constraints of movable type ("oh no, there are no more 'A's, we must rewrite this sentence!").

Fixed canvas sizes impact design options. ("That obviously obvious thing is obvious").

With DTP, the precise composition of the page is expected to be on purpose (web changes this due to browser incompatibilities/irregularities). In the example of irregular quotation marks being a mystery to us: was it a purposeful design decision or a practical solution to material constraint?

More importantly, would it be nice to re-introduce this mystery in some manner? Perhaps a shift to relationship-based design will introduce unexpected consequences

From Gijs: This idea of searching for or creating mystery is a bit kitsch. Frustrated by the idea that we need to explore glitches because our designs are too 'perfect'.

(In terms of the abherrent/odd/unexpected/deformed/"mysterious"): The material constraints of HTML/CSS is almost the opposite of the material factors in this Lissitzky example: whereas with movable type, the constraints are on the side of construction (availability of glyphs, font diversity, etc), in digital layout the material constraint is on the side of the presentation (screen size/resolution, software compatibility, etc).

Construction constraints remain in HTML/CSS, but the constraint of presentation has primacy: Internet Explorer 6 compatibility constrains more than the available options for construction precisely because presentation in IE6 constrains the options for construction (lowest common denominator).

In this way we can assert that a future tool should address these presentation-based constraints, transcending them similarly to how DTP transcended the contraints of construction.

A significant caveat: There is always a feedback loop between the material and the content. There is this brilliance piece by this writer who changes his text after seeing the printing proofs. The solution cannot be completely generative (not only layouts but content must be involved in a feedback loop).
What do we do about it now

Perhaps we could learn a great deal by re-implementing this design with the tools we have now. 
