VISUAL VERSIONING

aymeric will talk about sources, licenses

ana&ricardo will show how they use visual versionning (and how they don't use them)

text in wikipedia reader by INC
http://networkcultures.org/wpmu/portal/publications/inc-readers/critical-point-of-view-a-wikipedia-reader/

http://www.networkcultures.org/_uploads/%237reader_Wikipedia.pdf
--> page 94
THE POLITICS OF FORKING PATHS
NATHANIEL TKACZ <---- very nice
"""
What enables this perceived equivalence, this lossless quality of forking, resonates with what Wendy Chun describes as a ‘logic of “sourcery”’ found in recent attempts to grasp new media’s essence, by singling out what seems common to all: software. 31 For Chun, singling out software as the source of media is ‘a fetishism that obfuscates the vicissitudes of execution’. 32 This ‘sourcery’ also leads to the ‘valorisation of the user as agent’: 33 that is, the agential capacities of users are secured through their ability to know and manipulate the source. She writes:
These sourceries create a causal relationship among one’s actions, one’s code, and one’s interface. The relationship among code and interface, action and result, however, is always contingent and always to some extend imagined. The reduction of computer to source code, combined with the belief that users run our computers, makes us vulnerable to fantastic tales of the power of computing. 34
Singling out source also points to unique forms of epistemology and politics, and in particular Chun connects it to perceptions about the ‘the radicality of open source’. 35 If source is the essence of media and politics, open source, with its principles of access, visibility, modifiability, and, indeed, forkability (of the source), becomes the path to emancipation, or as Chopra and Dexter put it in the last line of their political treatise on software: ‘The technical is political: to free software is to free our selves’. 36 From this perspective, we can begin to fully appreciate political investments in forking and what underpins Weber’s remark, which might otherwise seem overstated: ‘the core freedom in free software is precisely and explicitly the right to fork’. Forking guarantees that everyone has full access to the magical source of freedom, power and enlightenment.
"""

ambiguity of the word checkout
git: to go back to a previous revision
svn: fetches remote content
diff and delta : what is the difference (or the delta!) between them

http://ospublish.constantvzw.org/tools/diff-git-imagemagick
http://www.bomahy.nl/hylke/blog/sparkleshare-goings-on/
http://stdin.fr/misc/font-wayback-machine_02.ogv
http://stdin.fr/misc/font-wayback-machine.ogv
http://git.constantvzw.org/?p=osp.marmite.git;a=tree
http://libregraphicsworld.org/blog/entry/gimp-rewritten-for-internal-version-control-support

Lisa is a developer working with collective video editing, and starting collaborative tools dev
Someone from madrid about collaboration

reader: talk about decentralized/centralized way of working. what does it change?
http://listes.domainepublic.net/pipermail/lgru-reader/2012-January/000035.html
geert lovink: "the principle of not working" 
http://networkcultures.org/wpmu/portal/publications/geert-lovink-publications/the-principle-of-notworking/ -> print copy at home!

Lightning talk: present OSP's workflow with git? OSP as a case between others

3 different examples of versioning tool use:
- OSP
- LGMagazine
- Lisa

OSP presentation during kitchen table presentations, not only about diff versioning
LGRU booklets: 50 copies at least  -> Ludi proposes her help (buy paper, layout, print)!
worksessions: 5/10 people
Eric wants to be concrete!

Worksession:
think practical. 
Denis Jacquerie will be there

OSP Kitchentable: 
other stuff
21:00 - OSP
22:00 - John

Nik : layout software history - why
Le tigre: more than a Scribus experience to offer. Eg. Their presence on the web, their imaginery etc.
how did they arrive to use Scribus?
why le tigre?
thier wishlist for the future
they used to be very active on the scribus mailing-list. not anymore, did they stabilize? found their own way?
they seem to use a lot of proprietary fonts
weekly pdf, monthly paper magazine to sell
which version of scribus
have they stop using it?
fonts
Tom : could you do some science fiction? sure!

Ludi-Antoine : box model - micro/macro - text into text - russian doll - filter and criterias - sieve - how the grid can be distorted - templated stuff (example of layouted political text as in 68) - opposed with criterias - the moment of collision, before, after, at the moment - autotamponeuse - elasticity - antoine is fan of astronomy, chaos with text block - non responsive design - the page will not rearrange > non web

ZUI (Zoomable User Interface) http://www.youtube.com/watch?feature=player_embedded&v=5JzaEUJ7IbE
distorting grid: http://www.poly-luna.com/3/grey-movie-grijsblok
http://www.poly-luna.com/5/argyle-pullover

Alan Kay videos: http://www.smalltalk.org.br/movies/

parametres, criteria

PierreH, Gijs: 
the page model: first class citizen or not
Talk together about page + glyph because this is two first class citizen that resist.
the space model
beyond the glyph bounding box model
the stroke - algorythm - metafont - gml (Graffiti markup language). (Parralèle with nietzsche and the typewriter story).

Knuth, elastic design from the shape structure, glyph is not only a rectangle, they are other shapes in this rectangle
underlying algorithms of metafont applied to the whole page
automatic kerning: they go beyond the box model, they look at the visual weigh of letters
frustrated by the page model: you need to decide the page format before hand. with conTeXt, you don't need to start from the page size
http://www.tug.org/utilities/plain/cseq.html

ludi has references how to set up/compose a dining table
layout, table compositions are there to avoid collisions
also setting up people around tables in a marriage/family dinner -> to avoid collisions!

LGRU reader: build a big file/classeur with photocopied texts that people can consult

Test reader. A5 a few texts to be given away to participants.
Syllabus style reader.

John: windowing history
Have a look at squeak!
http://books.google.be/books?id=psG_-FdOcfwC&pg=PA29&lpg=PA29&dq=squeak+red+car&source=bl&ots=D4vOZ0VbY_&sig=ZFxIN64ZqJuAqtb-yQBSsiuUFEQ&hl=fr&sa=X&ei=wcUvT5e-IoiG8gO0yPHwDg&ved=0CCQQ6AEwAA#v=onepage&q=squeak%20red%20car&f=false
http://en.wikipedia.org/wiki/Smalltalk

SHARED VOCABULARIES
Nicolas: SVG
Nathalie Trussart: philosopher working with Isabelle Stengers. she will follow the whole session. developing Imbroglio with Nicolas and Isabelle Stengers and Bruno Latour
http://www.imbroglio.be

Denis: foot on the typographic ground 

PierreM: presents something, not only lazy landscape. more about all this practice on sharing activities

John + PierreM: to be prepared

Shared vocabulary - lexicon - Steph + Michael: to be developed during Thursday lunch: http://lgru.pad.constantvzw.org:8000/43

mornings: be as wild as we can, speculate...!
afternoons: be concrete! Focused

Angela Plohman will manage a round table in Halles de Schaerbeek

---

PM a introduit la notion de text becoming text only when expressed on a support. Raw text is not yet a text, before being rendered legible for a human.
Question : what about semantic reading machines, like google bots?
It's a difficult notion, meaning of it is not already clear, but in that mist, it feeds intuitions

Tunisian intuition
So we try to distinguish what happen with the keystroke, the multiple transformations from these triggering to a chain of characters that can form words finally able to convey meaning.
Alas yes, these string uses blocks called letters, that cannot be opened in the layout processes, other that brute force outlined them.
What if we inform the whole process through typography and the history of the letter shapes?
Let's take a A is the head of a bull in the early script (jewish? Phenicians? to check) with the horizontal bar is the summit of the head. That drawing/icon has slowly been rotated, on the side at first, then 180°, on the horns. And generation of drawers have rendered this A with different inkstrokes. The one we use for the example is the Gill where that stroke seems to have loose all his genealogy of gesture. But in his thickness, or the ratio of his thickness and the thickness of the horns, something from the manual stroke remain. By example the direction, from left to right. If we rebuild the Gill Sans in an evaluated metafont - gml system, where the drawing is produced by strokes, informed by the intentions of Gill, informed by the origin and journey of the letter shape, and we use it to compose a short paragraph (or a whole book), we can instruct the program by example to modify the angle of the summit of the head of the bull according to the proximity of the margin - in reference of the boustrophedon - field analogy - a better example must be found, varying the level of the bar maybe.
More deeply, if the blank spaces, the various emptiness are also informed as necessary part of the lettering, as vital parts to convey meaning, we can rebuild a system of calligraphy, or simply a system of writing where the letters are not confined anymore in their bounding box prisons grid but move freely, in the flexible limits defined by custom or taboos (sex at dawn - letters orgy). We give back to letters the freedom that gutenberg have them taken out.
Example are difficult to found because our 600 year design frame has taken into account the rigidity of the lettering system. Calligraphy aside have orgasmically consumed the endless possibilities of weaving lettershapes. And graffitis.
Hypercontroled stonecutting have enjoyed some freedom in composition, but without any undo ability. Drawing students, any drawers, want to use their own writing along drawings; even if no mastering of lettering - not clear, but connected to education, la typography au tableau noir, hergé

What about a text composition system that, in case of more or less space like in the hyphenation process, ask letters to modify temporary their shapes to enjoy of the remaining space (the arabic horizontal bar is a brute example) or reduce deployment to accommodate the smallness of space / length of word ratio.

http://fr.wikipedia.org/wiki/Sto%C3%AFch%C3%A9don

PM - Idea of workshop
relations between characters
how to design the relationship between character?
how you layout the text engine layoutable for designers?
john - workshop - 
How relationships...


You have elements on a column
image frame
text frame
pragraph style

you have blocks on the page
you build relationships
propreties of the elements

example : you typeset a gif file
i want to go horizontal for each frame

setup
streams

the set can produced

